package com.alex.cordova.plugin;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import android.net.Uri;
import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.graphics.Point;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetector;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetectorOptions;
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.document.FirebaseVisionDocumentText;
import com.google.firebase.ml.vision.document.FirebaseVisionDocumentTextRecognizer;
import com.google.firebase.ml.vision.text.FirebaseVisionCloudTextRecognizerOptions;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Base64;
import com.google.android.gms.tasks.Task;
import java.io.UnsupportedEncodingException;

public class MLKitWrapper extends CordovaPlugin {
    private static final String TAG = "MLKitWrapper";
    private static Context context;

    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        context = cordova.getActivity().getApplicationContext();
        FirebaseApp.initializeApp(context);
    }

    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
        if (action.equals("barcode")) {
            System.out.println(args);
            final String img = args.getString(0);
            cordova.getThreadPool().execute(() -> readBarcode(callbackContext, img, "", true));
            return true;
        }

        if (action.equals("getText")) {
            final String img = args.getString(0);
            cordova.getThreadPool().execute(() -> runTextRecognition(callbackContext, img, "", false));
            return true;
        } else if (action.equals("getTextCloud")) {
            final String img = args.getString(0);
            final String lang = args.getString(1);
            Log.d(TAG, "LANGUAGE=" + lang);
            cordova.getThreadPool().execute(() -> runTextRecognition(callbackContext, img, lang, true));
            return true;
        }
        return false;
    }

    private JSONObject mapBoundingBox(Rect rect) throws JSONException {
      JSONObject oBloundingBox = new JSONObject();
      oBloundingBox.put("left", rect.left);
      oBloundingBox.put("right", rect.right);
      oBloundingBox.put("top", rect.top);
      oBloundingBox.put("bottom", rect.bottom);
      return oBloundingBox;
    }

    private JSONArray mapPoints(Point[] points) throws JSONException {
      JSONArray aPoints = new JSONArray();
      for (Point point: points) {
        JSONObject oPoint =  new JSONObject();
        oPoint.put("x", point.x);
        oPoint.put("y", point.y);
        aPoints.put(oPoint);
      }
      return aPoints;
    }

    private void readBarcode(final CallbackContext callbackContext, final String img, final String language, final Boolean onCloud) {
        System.out.println("inside parse barcode");
        System.out.println(img);
        FirebaseApp.initializeApp(context);
        FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(convertToBitmap(img));
        FirebaseVisionBarcodeDetectorOptions options = new FirebaseVisionBarcodeDetectorOptions.Builder()
        .setBarcodeFormats(
                FirebaseVisionBarcode.FORMAT_QR_CODE,
                FirebaseVisionBarcode.FORMAT_AZTEC,
                FirebaseVisionBarcode.FORMAT_DATA_MATRIX,
                FirebaseVisionBarcode.FORMAT_CODE_128,
                FirebaseVisionBarcode.FORMAT_CODE_39,
                FirebaseVisionBarcode.FORMAT_CODE_93,
                FirebaseVisionBarcode.FORMAT_CODABAR,
                FirebaseVisionBarcode.FORMAT_EAN_13,
                FirebaseVisionBarcode.FORMAT_EAN_8,
                FirebaseVisionBarcode.FORMAT_ITF,
                FirebaseVisionBarcode.FORMAT_UPC_A,
                FirebaseVisionBarcode.FORMAT_UPC_E,
                FirebaseVisionBarcode.FORMAT_PDF417)
        .build();

        FirebaseVisionBarcodeDetector detector = FirebaseVision.getInstance()
        .getVisionBarcodeDetector();


        Task<List<FirebaseVisionBarcode>> result = detector.detectInImage(image)
                .addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionBarcode>>() {
                    @Override
                    public void onSuccess(List<FirebaseVisionBarcode> barcodes) {
                        JSONArray arr = new JSONArray();
                        System.out.println("total barcodes ::: " + barcodes.size());
                        for (FirebaseVisionBarcode barcode: barcodes) {
                            Rect bounds = barcode.getBoundingBox();
                            Point[] corners = barcode.getCornerPoints();

                            String rawValue = barcode.getRawValue();

                            int valueType = barcode.getValueType();
                            JSONObject oBlock = new JSONObject(); 
                            try {  
                                System.out.println("*************** " + valueType + " ***************");
                                System.out.println(valueType);                         
                                switch (valueType) {
                                    case FirebaseVisionBarcode.TYPE_WIFI:
                                        System.out.println("************ it is WIFI");
                                        String ssid = barcode.getWifi().getSsid();
                                        String password = barcode.getWifi().getPassword();
                                        int type = barcode.getWifi().getEncryptionType();
                                        oBlock.put("ssid", ssid);
                                        oBlock.put("password", password);
                                        oBlock.put("type", type);
                                        oBlock.put("valueType", "WIFI");
                                        arr.put(oBlock);
                                        System.out.println(oBlock.toString());
                                        break;
                                    case FirebaseVisionBarcode.TYPE_URL:
                                        System.out.println("************ it is URL");
                                        String title = barcode.getUrl().getTitle();
                                        String url = barcode.getUrl().getUrl();
                                        oBlock.put("title", title);
                                        oBlock.put("url", url);
                                        oBlock.put("valueType", "URL");
                                        arr.put(oBlock);
                                        System.out.println(oBlock.toString());
                                        break;
                                    case FirebaseVisionBarcode.TYPE_CALENDAR_EVENT: 
                                        System.out.println("************ it is CALENDAR");
                                        String descr = barcode.getCalendarEvent().getDescription();
                                        String start = barcode.getCalendarEvent().getStart().toString();
                                        String end = barcode.getCalendarEvent().getEnd().toString();
                                        String organizer = barcode.getCalendarEvent().getOrganizer();
                                        String summary = barcode.getCalendarEvent().getSummary();
                                        String status = barcode.getCalendarEvent().getStatus();
                                        String location = barcode.getCalendarEvent().getLocation();
                                        oBlock.put("descr", descr);
                                        oBlock.put("start", start);
                                        oBlock.put("end", end);
                                        oBlock.put("organizer", organizer);
                                        oBlock.put("summary", summary);
                                        oBlock.put("status", status);
                                        oBlock.put("location", location);
                                        oBlock.put("valueType", "CALENDAR_EVENT");
                                        arr.put(oBlock);
                                        System.out.println(oBlock.toString());
                                        break;
                                    case FirebaseVisionBarcode.TYPE_CONTACT_INFO:
                                        System.out.println("************ it is CONTACT");
                                        //String addr = barcode.getContactInfo().getAddress();                                    
                                        String mail = barcode.getContactInfo().getEmails().toString();
                                        String phone = barcode.getContactInfo().getPhones().toString();
                                        String name = barcode.getContactInfo().getName().toString();
                                        String org = barcode.getContactInfo().getOrganization();
                                        String ttl = barcode.getContactInfo().getTitle().toString();
                                        String urlCI = barcode.getContactInfo().getUrls().toString();
                                        //oBlock.put("address", addr);
                                        oBlock.put("mail", mail);
                                        oBlock.put("phone", phone);
                                        oBlock.put("name", name);
                                        oBlock.put("organization", org);
                                        oBlock.put("title", ttl);
                                        oBlock.put("url", urlCI);
                                        oBlock.put("valueType", "CONTACT_INFO");
                                        arr.put(oBlock); 
                                        System.out.println(oBlock.toString());
                                        break;
                                    case FirebaseVisionBarcode.TYPE_DRIVER_LICENSE: 
                                        System.out.println("************ it is DRIVER LICENSE");
                                        String addrCity = barcode.getDriverLicense().getAddressCity();
                                        String addrState = barcode.getDriverLicense().getAddressState();
                                        String addrStreet = barcode.getDriverLicense().getAddressStreet();
                                        String addrZip = barcode.getDriverLicense().getAddressZip();
                                        String birthDate = barcode.getDriverLicense().getBirthDate();
                                        String docType = barcode.getDriverLicense().getDocumentType();
                                        String expiryDate = barcode.getDriverLicense().getExpiryDate();
                                        String firstName = barcode.getDriverLicense().getFirstName();
                                        String midName = barcode.getDriverLicense().getMiddleName();
                                        String lastName = barcode.getDriverLicense().getLastName();
                                        String gender = barcode.getDriverLicense().getGender();
                                        String issueDate = barcode.getDriverLicense().getIssueDate();
                                        String issueCountry = barcode.getDriverLicense().getIssuingCountry();
                                        String licenseNum = barcode.getDriverLicense().getLicenseNumber();
                                        oBlock.put("getAddressCity", addrCity);
                                        oBlock.put("addressState", addrState);
                                        oBlock.put("addressStreet", addrStreet);
                                        oBlock.put("addressZip", addrZip);
                                        oBlock.put("birthDate", birthDate);
                                        oBlock.put("documentType", docType);
                                        oBlock.put("expiryDate", expiryDate);
                                        oBlock.put("firstName", firstName);
                                        oBlock.put("midName", midName);
                                        oBlock.put("lastName", lastName);
                                        oBlock.put("gender", gender);
                                        oBlock.put("issueDate", issueDate);
                                        oBlock.put("issueCountry", issueCountry);
                                        oBlock.put("issueCountry", issueCountry);
                                        oBlock.put("licenseNumber", "licenseNum");
                                        oBlock.put("valueType", "DRIVER_LICENSE");
                                        arr.put(oBlock); 
                                        System.out.println(oBlock.toString());
                                        break;
                                    case FirebaseVisionBarcode.TYPE_EMAIL: 
                                        System.out.println("************ it is EMAIL");
                                        int typeMail = barcode.getEmail().getType();
                                        String address = barcode.getEmail().getAddress();
                                        String body = barcode.getEmail().getBody();
                                        String subject = barcode.getEmail().getSubject();
                                        oBlock.put("type", typeMail);
                                        oBlock.put("address", address);
                                        oBlock.put("body", body);
                                        oBlock.put("subject", subject);
                                        oBlock.put("valueType", "EMAIL");
                                        arr.put(oBlock);
                                        System.out.println(oBlock.toString());
                                        break;
                                    case FirebaseVisionBarcode.TYPE_GEO: 
                                        System.out.println("************ it is GEO");
                                        String lat = Double.toString(barcode.getGeoPoint().getLat());
                                        String lng = Double.toString(barcode.getGeoPoint().getLng());
                                        oBlock.put("latitude", lat);
                                        oBlock.put("longitude", lng);
                                        oBlock.put("valueType", "GEO");
                                        arr.put(oBlock);
                                        System.out.println(oBlock.toString());
                                        break;
                                    case FirebaseVisionBarcode.TYPE_PHONE: 
                                        System.out.println("************ it is PHONE");
                                        String num = barcode.getPhone().getNumber();
                                        int typePhone = barcode.getPhone().getType();
                                        oBlock.put("number", num);
                                        oBlock.put("type", typePhone);
                                        oBlock.put("valueType", "GEO");
                                        arr.put(oBlock);
                                        System.out.println(oBlock.toString());
                                        break;
                                    case FirebaseVisionBarcode.TYPE_SMS:
                                        System.out.println("************ it is SMS"); 
                                        //String numSms = barcode.getSms().getNumber();
                                        String msg = barcode.getSms().getMessage();
                                        //oBlock.put("number", numSms);
                                        oBlock.put("message", msg);
                                        oBlock.put("valueType", "GEO");
                                        arr.put(oBlock);
                                        System.out.println(oBlock.toString());
                                        break;
                                }
                            } catch(Exception e) {
                                System.out.println("ERROR !!!!!!!");
                                System.out.println(e);
                            }
                        }
                        callbackContext.success(arr);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        callbackContext.error(e.getMessage());
                    }
                });
    }

    private void runTextRecognition(final CallbackContext callbackContext, final String img, final String language, final Boolean onCloud) {
        Uri imgSource = Uri.parse(img);

        try {
            FirebaseVisionImage image = FirebaseVisionImage.fromFilePath(cordova.getContext(), imgSource);            
            FirebaseVisionTextRecognizer textRecognizer;

            if(onCloud) {
                textRecognizer = this.getTextRecognitionCloud(language);
            } else {
                textRecognizer = this.getTextRecognitionDevice();
            }

            textRecognizer.processImage(image).addOnSuccessListener(texts -> {
              try {
                JSONObject json = new JSONObject();
                JSONArray blocks = new JSONArray();

                json.put("text", texts.getText());
                json.put("textBlocks", blocks);

                for (FirebaseVisionText.TextBlock block : texts.getTextBlocks()) {
                    Log.d(TAG, block.getText());
                    JSONObject oBlock = new JSONObject();
                    JSONArray lines = new JSONArray();
                    oBlock.put("text", block.getText());
                    oBlock.put("confidence", block.getConfidence());
                    oBlock.put("boundingBox", mapBoundingBox(block.getBoundingBox()));
                    oBlock.put("cornerPoints", mapPoints(block.getCornerPoints()));
                    oBlock.put("lines", lines);
                    blocks.put(oBlock);

                    for (FirebaseVisionText.Line line : block.getLines()) {
                      JSONObject oLine = new JSONObject();
                      oLine.put("text", line.getText());
                      oLine.put("confidence", line.getConfidence());
                      oLine.put("boundingBox", mapBoundingBox(line.getBoundingBox()));
                      oLine.put("cornerPoints", mapPoints(line.getCornerPoints()));
                      lines.put(oLine);
                    }
                }
                callbackContext.success(json);
              } catch(JSONException e) {
                e.printStackTrace();
                callbackContext.error(e.getMessage());
              }
            }).addOnFailureListener(e -> {
                // Task failed with an exception
                e.printStackTrace();
                callbackContext.error(e.getMessage());
            });
        } catch (IOException e) {
            e.printStackTrace();
            callbackContext.error(e.getMessage());
        }

    }

    private FirebaseVisionTextRecognizer getTextRecognitionDevice() {
        return FirebaseVision.getInstance().getOnDeviceTextRecognizer();
    }

    private FirebaseVisionTextRecognizer getTextRecognitionCloud( final String language) {
        if (!language.isEmpty()) {
            FirebaseVisionCloudTextRecognizerOptions options = new FirebaseVisionCloudTextRecognizerOptions.Builder()
                    .setLanguageHints(Arrays.asList(language)).build();

            return FirebaseVision.getInstance()
                    .getCloudTextRecognizer(options);
        } else {
            return FirebaseVision.getInstance().getCloudTextRecognizer();
        }
    }

    private Bitmap convertToBitmap(String imgData) {
        System.out.println("will convert to bitmap");
        System.out.println(new String(imgData));
        byte[] decodedString = Base64.getDecoder().decode(imgData);
        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        return decodedByte;
    }

    private byte[] convertToByteArray(String imgData) {
        try {
            //byte[] name = Base64.encodeBase64(imgData.getBytes());
            //byte[] decodedString = Base64.decodeBase64(new String(name).getBytes("UTF-8"));
            byte[] decodedString = Base64.getDecoder().decode(imgData);            
            return decodedString;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return new byte[0];
        }
    }

}