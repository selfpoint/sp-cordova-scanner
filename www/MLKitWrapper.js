var exec = require("cordova/exec");

const MLKitWrapper = "MLKitWrapper";

/**
 * @description
 * Parse barcode from taken image
 *
 * @example
 * cordova.plugins.MLKitWrapper.parseBarcode(["app_test_1"], _onSuccess, _onFailure);
 *
 * @param  {array} args - Leave as Empty Array
 * @param  {callback} success - A Cordova-style success callback object.
 * @param  {callback} error - A Cordova-style error callback object.
 */
exports.parseBarcode = function (args, success, error) {
	CameraPreview.takePicture(function(base64PictureData){
  		exec(success, error, MLKitWrapper, "barcode", base64PictureData);
	});    
};

exports.parseBarcodeData = function (args, success, error) {
	exec(success, error, MLKitWrapper, "barcode", args);   
};

/**
 * @description
 * initialize the Camera
 *
 * @param  {array} args - Object holding the initial settings for the Camera
 * @param  {callback} success - A Cordova-style success callback object.
 * @param  {callback} error - A Cordova-style error callback object.
 */
exports.startMLCamera = function (args, success, error) {	
    CameraPreview.startCamera(args);
    success('started');
};

/**
 * @description
 * Show the Camera
 *
 * @param  {array} args - Leave as Empty Array
 * @param  {callback} success - A Cordova-style success callback object.
 * @param  {callback} error - A Cordova-style error callback object.
 */
exports.showCamera = function (args, success, error) {	
    CameraPreview.show();
    success('shown');
};

/**
 * @description
 * Hide the Camera
 *
 * @param  {array} args - Leave as Empty Array
 * @param  {callback} success - A Cordova-style success callback object.
 * @param  {callback} error - A Cordova-style error callback object.
 */
exports.hideCamera = function (args, success, error) {	
    CameraPreview.hide();
    success('hidden');
};

/**
 * @description
 * Stop the Camera
 *
 * @param  {array} args - Leave as Empty Array
 * @param  {callback} success - A Cordova-style success callback object.
 * @param  {callback} error - A Cordova-style error callback object.
 */
exports.stopCamera = function (args, success, error) {
	CameraPreview.stopCamera();
	success('stopped');
};

/**
 * @description
 * Alter the Camera preview size
 *
 * @param  {array} args - Object holding the new width and height of the preview window
 * @param  {callback} success - A Cordova-style success callback object.
 * @param  {callback} error - A Cordova-style error callback object.
 */
exports.alterPreviewSize = function (args, success, error) {
	CameraPreview.setPreviewSize({width: args.width, height: args.height});
	success('stopped');
}